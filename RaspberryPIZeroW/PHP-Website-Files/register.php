<?php
 // Include database config file
 require_once "DB_config.php";
 
// Define variables and initialize with empty values
$login_username = $login_password = $confirm_login_password = "";
$login_username_err = $login_password_err = $confirm_login_password_err = "";

// Setup connection to mysql database used for login credentials
$DB_conn = mysqli_connect($mysql_servername, $mysql_username, $mysql_password, $mysql_db_name);
if(!$DB_conn)
{
    echo "FAILED TO CONNECT TO DATABASE\n";
}
//else
//{
//    echo "Successfully connected to database.\n";
//}

// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    // Validate username
    if(empty(trim($_POST["username"]))){
        $login_username_err = "Please enter a username.";
    } else{
        // Prepare a select statement
        $sql = "SELECT id FROM users WHERE username = ?";
        
        if($stmt = $DB_conn->prepare($sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            
            // Set parameters
            $param_username = trim($_POST["username"]);
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                /* store result */
                mysqli_stmt_store_result($stmt);
                
                if(mysqli_stmt_num_rows($stmt) == 1){
                    $login_username_err = "This username is already taken.";
                } else{
                    $login_username = trim($_POST["username"]);
                }
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }
         
        // Close statement
        mysqli_stmt_close($stmt);
    }
    
    // Validate password
    if(empty(trim($_POST["password"]))){
        $login_password_err = "Please enter a password.";     
    } elseif(strlen(trim($_POST["password"])) < 6){
        $login_password_err = "Password must have atleast 6 characters.";
    } else{
        $login_password = trim($_POST["password"]);
    }
    
    // Validate confirm password
    if(empty(trim($_POST["confirm_password"]))){
        $confirm_login_password_err = "Please confirm password.";     
    } else{
        $confirm_login_password = trim($_POST["confirm_password"]);
        if(empty($login_password_err) && ($login_password != $confirm_login_password)){
            $confirm_login_password_err = "Password did not match.";
        }
    }
    
    // Check input errors before inserting in database
    if(empty($login_username_err) && empty($login_password_err) && empty($confirm_login_password_err)){
        // Prepare an insert statement
        $sql = "INSERT INTO users (username, password) VALUES (?, ?)";
         
        if($stmt = $DB_conn->prepare($sql)){
            
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "ss", $param_username, $param_password);
            
            // Set parameters
            $param_username = $login_username;
            $param_password = password_hash($login_password, PASSWORD_DEFAULT); // Creates a password hash
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Redirect to login page
                header("location: login.php");
            } else{
            }
        }
        
        // Close statement
        mysqli_stmt_close($stmt);
    }
    
    // Close connection
    mysqli_close($link);
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sign Up</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>
</head>
<body>
    <div class="wrapper">
        <h2>Sign Up</h2>
        <p>Please fill this form to create an account.</p>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group <?php echo (!empty($login_username_err)) ? 'has-error' : ''; ?>">
                <label>Username</label>
                <input type="text" name="username" class="form-control" value="<?php echo $login_username; ?>">
                <span class="help-block"><?php echo $login_username_err; ?></span>
            </div>    
            <div class="form-group <?php echo (!empty($login_password_err)) ? 'has-error' : ''; ?>">
                <label>Password</label>
                <input type="password" name="password" class="form-control" value="<?php echo $login_password; ?>">
                <span class="help-block"><?php echo $login_password_err; ?></span>
            </div>
            <div class="form-group <?php echo (!empty($confirm_login_password_err)) ? 'has-error' : ''; ?>">
                <label>Confirm Password</label>
                <input type="password" name="confirm_password" class="form-control" value="<?php echo $confirm_login_password; ?>">
                <span class="help-block"><?php echo $confirm_login_password_err; ?></span>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Submit">
                <input type="reset" class="btn btn-default" value="Reset">
            </div>
            <p>Already have an account? <a href="login.php">Login here</a>.</p>
        </form>
    </div>    
</body>
</html>
