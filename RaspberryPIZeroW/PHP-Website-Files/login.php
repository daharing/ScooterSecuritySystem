<?php
// Include database config file
require_once "DB_config.php";

// Initialize the session
session_start();

// Check if the user is already logged in, if yes then redirect him to welcome page
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true ){
    header("location: welcome.php");
    $DB_conn->close();
    exit;
}

// Setup connection to mysql database used for login credentials
$DB_conn = mysqli_connect($mysql_servername, $mysql_username, $mysql_password, $mysql_db_name);
if(!$DB_conn)
{
    echo "FAILED TO CONNECT TO DATABASE\n";
}
//else
//{
//    echo "Successfully connected to database.\n";
//}

$sql = "select username, password from users";

$result = mysqli_query($DB_conn, $sql);

// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){


    // Check if username is empty
    if(empty(trim($_POST["username"]))){
	$login_username_err = "Please enter username.";
    } else{
        $login_username = trim($_POST["username"]);
    }

    // Check if password is empty
    if(empty(trim($_POST["password"]))){
        $login_password_err = "Please enter your password.";
    } else{
        $login_password = trim($_POST["password"]);
    }

    // Validate credentials
    if(empty($login_username_err) && empty($login_password_err)){

        // Prepare a select statement
        $sql = "SELECT id, username, password FROM users WHERE username = ?";

	if($stmt = $DB_conn->prepare($sql)){

            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_username);

            // Set parameters
            $param_username = $login_username;

            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){

                // Store result
                mysqli_stmt_store_result($stmt);

                // Check if username exists, if yes then verify password
                if(mysqli_stmt_num_rows($stmt) == 1){

                    // Bind result variables
		    $hashed_password = $login_password;
                    mysqli_stmt_bind_result($stmt, $id, $login_username, $hashed_password);
                    if(mysqli_stmt_fetch($stmt)){

		            if(password_verify($login_password, $hashed_password)){
                            	// Password is correct, so start a new session
                            	session_start();
	                        // Store data in session variables
                            	$_SESSION["loggedin"] = true;
                            	$_SESSION["id"] = $id;
                            	$_SESSION["username"] = $login_username;

                            // Redirect user to welcome page
                            header("location: welcome.php");
                        } else{
                            // Display an error message if password is not valid
                            $login_password_err = "The password you entered was not valid.";
                        }
                    }
                } else{
                    // Display an error message if username doesn't exist
                    $login_username_err = "No account found with that username.";
                }
            } else{
                echo "Oops! Something went wrong. Please try again later.\n";
            }
        }
        // Close statement
        mysqli_stmt_close($stmt);
    }

    // Close connection
    mysqli_close($link);
}
?>

<!Doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>
</head>
<body>
    <div class="wrapper">
        <h2>Login</h2>
        <p>Please fill in your credentials to login.</p>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group <?php echo (!empty($login_username_err)) ? 'has-error' : ''; ?>">
                <label>Username</label>
                <input type="text" name="username" class="form-control" value="<?php echo $login_username; ?>">
                <span class="help-block"><?php echo $login_username_err; ?></span>
            </div>
            <div class="form-group <?php echo (!empty($login_password_err)) ? 'has-error' : ''; ?>">
                <label>Password</label>
                <input type="password" name="password" class="form-control">
                <span class="help-block"><?php echo $login_password_err; ?></span>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Login">
            </div>
            <p>Don't have an account? <a href="register.php">Sign up now</a>.</p>
        </form>
    </div>
</body>
</html>
