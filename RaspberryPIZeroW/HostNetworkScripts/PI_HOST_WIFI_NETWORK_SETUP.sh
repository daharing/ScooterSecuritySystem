#!/bin/bash

#install dependencies
sudo apt install dnsmasq hostapd -y

#stop services
sudo systemctl stop dnsmasq
sudo systemctl stop hostapd

#Backup Files that will be modified
sudo cp /etc/dhcpcd.conf /etc/dhcpcd.conf_original
sudo cp /etc/dnsmasq.conf /etc/dnsmasq.conf_original
sudo cp /etc/hostapd/hostapd.conf /etc/hostapd/hostapd.conf_original
sudo cp /etc/default/hostapd /etc/default/hostapd_original

#Append the following to /etc/dhcpcd.conf:
sudo bash -c 'echo "
interface wlan0
    static ip_address=192.168.200.1/24 #not sure what the /24 does but it is required and is independant of the static ip address
    static routers=192.168.200.1
    static domain_name_servers=192.168.200.1
    nohook wpa_supplicant
" >> /etc/dhcpcd.conf'

#Append the following to /etc/dnsmasq.conf:
sudo bash -c 'echo "
interface=wlan0 
 dhcp-range=wlan0,192.168.200.2,192.168.200.10,255.255.255.0,24h" >> /etc/dnsmasq.conf'

#Append the following to /etc/hostapd/hostapd.conf
sudo bash -c 'echo "interface=wlan0
ssid=ScooterWireless
wpa_passphrase=M0124607
#driver=nl80211 #shouldnt be required to specify
hw_mode=g
#hw_mode=a #sets wireless to be 5GHz instead. Not supported by PI
channel=7
wmm_enabled=0
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
" >> /etc/hostapd/hostapd.conf'

#Append/modify the following to /etc/default/hostapd
sudo bash -c 'echo "DAEMON_CONF=\"/etc/hostapd/hostapd.conf\"" >> /etc/default/hostapd'

sudo systemctl daemon-reload
sudo service dhcpcd restart

sudo reboot
