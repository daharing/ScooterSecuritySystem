#!/bin/bash

#Delete the backup files
sudo rm /etc/dhcpcd.conf_original
sudo rm /etc/dnsmasq.conf_original 
sudo rm /etc/hostapd/hostapd.conf_original
sudo rm  /etc/hostapd_original

#purge dnsmasq and hostapd which were installed in the setup script
sudo apt purge dnsmasq hostapd -y
sudo apt autoclean
sudo apt autoremove

#reload the dhcpcd service 
sudo systemctl daemon-reload
sudo service dhcpcd restart

#restart computer
sudo reboot
